from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView, \
    CreateView, UpdateView, \
    DeleteView
from rest_framework.reverse import reverse

from .models import Attachment


# Create your views here.

class AttachmentListView(ListView):
    model = Attachment
    template_name = "attachment_list.html"
    context_object_name = "attachments"

    def get_context_data(self, **kwargs):
        context = super(AttachmentListView, self).get_context_data(**kwargs)
        if not self.request.user.is_anonymous:
            context["attachments"] = Attachment.objects.filter(user=self.request.user)
        return context


class AttachmentCreateView(CreateView):
    model = Attachment
    template_name = "attachment_create.html"
    context_object_name = "attachment"
    fields = ["file", ]
    success_url = reverse_lazy("attachment_list")

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.save()
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER'))


class AttachmentDetailView(DetailView):
    model = Attachment
    template_name = "attachment_detail.html"
    context_object_name = "attachment"
    fields = "__all__"


class AttachmentUpdateView(UpdateView):
    model = Attachment
    template_name = "attachment_update.html"
    context_object_name = "attachment"
    fields = "__all__"
    success_url = reverse_lazy("attachment_list")


class AttachmentDeleteView(DeleteView):
    model = Attachment
    template_name = "attachment_delete.html"
    context_object_name = "attachment"
    fields = "__all__"
    success_url = reverse_lazy("attachment_list")

    def post(self, request, *args, **kwargs):
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('attachment_list'))
        else:
            return super().post(request, *args, **kwargs)
