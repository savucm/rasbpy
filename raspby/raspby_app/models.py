from django.contrib.auth.models import User
from django.db import models


# Create your models here.

class Attachment(models.Model):
    file = models.FileField(upload_to='static')
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.file.name

# class GroupUser(models.Model):
#     nume = models.CharField(max_length=50)
#     grupa_clasa = models.CharField(max_length=200)
