from django.urls import path
# from django.views import View
from .views import (AttachmentListView, AttachmentCreateView,
                    AttachmentDetailView, AttachmentUpdateView, \
                    AttachmentDeleteView, )

urlpatterns = [
    # path('', Views.py(), name="attachament_list"),
    path('', AttachmentListView.as_view(), name="attachment_list"),
    path('create/', AttachmentCreateView.as_view(), name="attachment_create"),
    path('detail/<int:pk>', AttachmentDetailView.as_view(), name="attachment_detail"),
    path('update/<int:pk>', AttachmentUpdateView.as_view(), name="attachment_update"),
    path('delete/<int:pk>', AttachmentDeleteView.as_view(), name="attachment_delete"),
]
