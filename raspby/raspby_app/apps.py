from django.apps import AppConfig


class RaspbyAppConfig(AppConfig):
    name = 'raspby_app'
