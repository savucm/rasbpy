from django.contrib import admin

# Register your models here.
from raspby_app.models import Attachment

admin.site.register(Attachment)