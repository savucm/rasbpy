from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from .models import Product, Category
from django.http import HttpResponseRedirect
from .forms import MyForm
from products.serializers import ProductSerializer, CategorySerializer
from rest_framework import viewsets

# Create your views here.

class ProductListView(ListView):
    model = Product
    template_name = "product_list.html"
    context_object_name = "products"

class ProductCreateView(CreateView):
    model = Product
    template_name = "product_create.html"
    context_object_name = "products"
    fields = "__all__"
    success_url = reverse_lazy("product_list") # aici e link-ul unde ne duce cand completam form-ul

class ProductDetailView(DetailView):
    model = Product
    template_name = "product_detail.html"
    context_object_name = "products"

class ProductUpdateView(UpdateView):
    model = Product
    template_name = "product_update.html"
    context_object_name = 'products'
    fields = "__all__"
    success_url = reverse_lazy("product_list")

    def post(self, request, pk):
        # print(request)
        product = Product.objects.filter(pk=pk)
        items = request.POST.dict()
        del items['csrfmiddlewaretoken']
        product.update(**items)
        return HttpResponseRedirect(reverse('product_detail', args=(pk,)))

class ProductDeleteView(DeleteView):
    model = Product
    template_name = 'product_delete.html'
    content_object_name = 'product'
    success_url = reverse_lazy('product_list')

    def post(self, request, *args, **kwargs):
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('product_list'))
        else:
            return super().post(request, *args, **kwargs)


class CategoryListView(ListView):
    model = Category
    template_name = 'category_list.html'
    context_object_name = 'categories'


class CategoryDetailView(DetailView):
    model = Category
    template_name = 'category_detail.html'
    context_object_name = 'category'


class CategoryCreateView(CreateView):
    model = Category
    template_name = 'category_create.html'
    context_object_name = 'category'
    success_url = reverse_lazy('category_list')
    fields = '__all__'


class CategoryUpdateView(UpdateView):
    model = Category
    template_name = "category_update.html"
    context_object_name = "category"
    fields = '__all__'
    success_url = reverse_lazy('category_list')


class CategoryDeleteView(DeleteView):
    model = Category
    template_name = "category_delete.html"
    context_object_name = "category"
    fields = '__all__'
    success_url = reverse_lazy('category_list')

    def post(self, request, *args, **kwargs):
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('category_list'))
        else:
            return super().post(request, *args, **kwargs)


def my_form(request):
    if request.method == 'POST':
        form = MyForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/thank-you')
        else:
            return render(request, 'my_form.html', {'form': form})
    else:
        form = MyForm()

    return render(request, 'my_form.html', {'form': form})


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all().order_by('name')
    serializer_class = ProductSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


