from django import forms

class MyForm(forms.Form):
    name = forms.CharField(label='name', max_length=30)
    city = forms.CharField(label='City', max_length=25)
    CHOICES = [('yes', 'Yes, I am happy!'),
               ('no', 'No, I am not happy.')]
    is_happy = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)
    hobby = forms.CharField(label='Hobby', max_length=30)

    def clean(self):
        super(MyForm, self).clean()

        name = self.cleaned_data.get('name')
        city = self.cleaned_data.get('city')

        for character in name:
            if character.isdigit():
                self.errors['name'] = self.error_class([
                    'No digits allowed, you are not Kanye West.'])

        for character in name:
            if character.isdigit():
                self.errors['city'] = self.error_class([
                    'I said city, not postcode!.'])

        return self.cleaned_data
