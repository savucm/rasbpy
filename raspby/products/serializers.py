from rest_framework import serializers
from products.models import Product, Category


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ['name', 'type']

class ProductSerializer(serializers.HyperlinkedModelSerializer):
    category = CategorySerializer
    class Meta:
        model = Product
        fields = '__all__'