from django.urls import path, include
from .views import (ProductListView, ProductCreateView,
                    ProductDetailView, ProductUpdateView,
                    ProductDeleteView, CategoryListView,
                    CategoryDetailView, CategoryCreateView,
                    CategoryUpdateView, CategoryDeleteView,
                    my_form, ProductViewSet, CategoryViewSet)

from rest_framework import routers

from products.views import ProductViewSet, CategoryViewSet

router = routers.DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'categories', CategoryViewSet)

urlpatterns = [
    path('list/', ProductListView.as_view(), name="product_list"),
    path('', ProductListView.as_view(), name="product_list"),
    path('create/', ProductCreateView.as_view(), name="product_create"),
    path('detail/<int:pk>', ProductDetailView.as_view(), name="product_detail"),
    path('update/<int:pk>', ProductUpdateView.as_view(), name="product_update"),
    path('delete/<int:pk>', ProductDeleteView.as_view(), name='product_delete'),
    path('category/', CategoryListView.as_view(), name='category_list'),
    path('category/create/', CategoryCreateView.as_view(), name="category_create"),
    path('category/detail/<int:pk>', CategoryDetailView.as_view(), name='category_detail'),
    path('category/update/<int:pk>', CategoryUpdateView.as_view(), name="category_update"),
    path('category/delete/<int:pk>', CategoryDeleteView.as_view(), name="category_delete"),
    path('form/', my_form, name='my_form'),
    # path('api/', include((router.urls, 'rest_framework.urls'), namespace='rest_framework')),
]
