from django.urls import path

from pages.views import home_page_view, AboutView

urlpatterns = [
    path('', home_page_view, name = 'home'),
    # path('likes/<int:blog_id>', likes_counter, name='likes'),
    path('about/', AboutView.as_view())
]