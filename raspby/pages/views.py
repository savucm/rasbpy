from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from datetime import datetime
from django.views.generic import TemplateView

# Create your views here.

def home_page_view(request):
    return HttpResponse('Hello, world!')


def home_page_view(request):
    #add_post_age_data(blog_post)
    return render(request, "base.html", {})


def likes_counter(request, blog_id):
    # for post in blog_post["blog_entries"]:
    #     if post["id"] == blog_id:
    #         post["likes"] += 1
    return HttpResponseRedirect('/')

class AboutView(TemplateView):
    template_name = "about.html"